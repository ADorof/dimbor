			<footer class="footer" role="contentinfo" itemscope itemtype="http://schema.org/WPFooter">

				<div id="inner-footer" class="wrap cf">

					<nav role="navigation">
						<?php wp_nav_menu(array(
    					'container' => 'div',                           // enter '' to remove nav container (just make sure .footer-links in _base.scss isn't wrapping)
    					'container_class' => 'footer-links cf',         // class of container (should you choose to use it)
    					'menu' => __( 'Footer Links', 'bonestheme' ),   // nav name
    					'menu_class' => 'nav footer-nav cf',            // adding custom nav class
    					'theme_location' => 'footer-links',             // where it's located in the theme
    					'before' => '',                                 // before the menu
    					'after' => '',                                  // after the menu
    					'link_before' => '',                            // before each link
    					'link_after' => '',                             // after each link
    					'depth' => 0,                                   // limit the depth of the nav
    					'fallback_cb' => 'bones_footer_links_fallback'  // fallback function
						)); ?>
					</nav>

					<div class="inner-content clearfix">
						<div class="m-all t-1of2 d-1of4">
							<div class="footer-caption address white">
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/map.png" class="pull-left" alt="">
								<p>Адрес</p>
							</div>
							<address>
								<?php dynamic_sidebar('address'); ?>
							</address>
						</div>
						<div class="m-all t-1of2 d-1of4">
							<div class="footer-caption phone white">
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/phone.png" class="pull-left" alt="">
								<p>Телефоны</p>
							</div>
							<address>
								<?php dynamic_sidebar('phone-1'); ?>
								<?php dynamic_sidebar('phone-2'); ?>
							</address>
						</div>
						<div class="m-all t-1of2 d-1of4">
							<div class="footer-caption time white">
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/clock.png" class="pull-left" alt="">
								<p>Время работы</p>
							</div>
							<address>
								<?php dynamic_sidebar('time'); ?>
							</address>
						</div>
						<div class="m-all t-1of2 d-1of4">
							<div class="footer-caption email white">
								<img src="<?php echo get_template_directory_uri(); ?>/library/images/email.png" class="pull-left" alt="">
								<p>E-MAIL</p>
							</div>
							<address>
								<?php dynamic_sidebar('email'); ?>
							</address>
						</div>
					</div>

				</div>
				<div class="map-footer">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2942.415974181544!2d26.063206416144602!3d53.13119817993447!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x46d8daa9c22c52c1%3A0xd901620d4fd32e68!2z0J7QntCeICLQlNC40LzQsdC-0YAt0LvQvtC6Ig!5e1!3m2!1sru!2sru!4v1482755362882" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
			</footer>

		</div>

		<?php // all js scripts are loaded in library/bones.php ?>
		<?php wp_footer(); ?>
			<!-- Yandex.Metrika counter --><script type="text/javascript">(function (d, w, c) { (w[c] = w[c] || []).push(function() { try { w.yaCounter28183980 = new Ya.Metrika({id:28183980, webvisor:true, clickmap:true, trackLinks:true, accurateTrackBounce:true}); } catch(e) { } }); var n = d.getElementsByTagName("script")[0], s = d.createElement("script"), f = function () { n.parentNode.insertBefore(s, n); }; s.type = "text/javascript"; s.async = true; s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js"; if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); } })(document, window, "yandex_metrika_callbacks");</script><noscript><div><img src="//mc.yandex.ru/watch/28183980" style="position:absolute; left:-9999px;" alt="" /></div></noscript><!-- /Yandex.Metrika counter -->
	</body>

</html> <!-- end of site. what a ride! -->
