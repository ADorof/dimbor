<?php get_header(); ?>

			<div id="content">

				<?php echo do_shortcode("[slick-slider category=\"9\" sliderheight=\"auto\" speed=\"1000\"]"); ?>

				<div id="inner-content" class="wrap cf">

						<main id="main" class="m-all t-all d-all cf" role="main" itemscope itemprop="mainContentOfPage" itemtype="http://schema.org/Blog">

							<h1 class="text-center">Компания "<?php bloginfo('name'); ?>" предлагает:</h1>
							<?php query_posts('meta_key=catalog&meta_value=1') ?>
							<?php if (have_posts()) : while (have_posts()) : the_post(); ?>

								<div class="m-all t-1of2 d-1of3 wrap-thumb">
									<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php the_title_attribute(); ?>">
										<div class="thumb">
											<?php the_post_thumbnail(array(334,240)) ?>
											<div class="caption-thumb text-center">
												<p class="h2"><?php the_title(); ?></p>
											</div>
										</div>
									</a>
								</div>

							<?php endwhile; ?>

							<?php endif; ?>


						</main>

				</div>

			</div>


<?php get_footer(); ?>
